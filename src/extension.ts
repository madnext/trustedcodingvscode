import * as vscode from 'vscode';
import fetch, * as http from 'node-fetch';
import { Context, Script } from 'vm';

const diagnosticCollections: vscode.DiagnosticCollection[] = [];
const decoratorsCollections: vscode.TextEditorDecorationType[] = [];

export function activate(context: vscode.ExtensionContext) {
	const disposable = async () => {
		const crypto = require("crypto");
		if (context.globalState.get('user') === undefined) { context.globalState.update('user', crypto.randomBytes(24).toString('hex')); }
		if (context.globalState.get('depth') === undefined) { context.globalState.update('depth', false); }
		if (context.globalState.get('references') === undefined) { context.globalState.update('references', false); }
		if (context.globalState.get('levels') === undefined) { context.globalState.update('levels', []); }
		if (context.globalState.get('tool') === undefined) { context.globalState.update('tool', []); }
		if (context.globalState.get('opener') === undefined) { context.globalState.update('opener', false); }
		if (context.globalState.get('time') === undefined) { context.globalState.update('time', false); }
		if (context.globalState.get('time_time') === undefined) { context.globalState.update('script_time', 0); }
		if (context.globalState.get('experience') === undefined) { context.globalState.update('experience', false); }
		if (context.globalState.get('experience_time') === undefined) { context.globalState.update('experience_time', 0); }
		parse_call(context);
	};

	const clearResults = async () => {
		const editor = vscode.window.activeTextEditor;
		if (!editor) { return; }
		for (const col of diagnosticCollections) { col.clear(); }
		for (const dec of decoratorsCollections) { dec.dispose(); }
	};

	const openBrowser = () => {
		var report_uri = "https://trustedcoding-ai.pre.inftel.madisonmk.com/reports/user";
		var user_id = context.globalState.get('user');
		vscode.env.openExternal( vscode.Uri.parse(report_uri + "/" + user_id) );
	}

	const setOpenerContext = async (isOpener: boolean) => { vscode.commands.executeCommand('setContext', 'extension:isOpener', isOpener); };
	const setDepthContext = async (isDepth: boolean) => { vscode.commands.executeCommand('setContext', 'extension:isDepth', isDepth); };
	const setReferencesContext = async (isReferences: boolean) => { vscode.commands.executeCommand('setContext', 'extension:isReferences', isReferences); };
	const setTimeContext = async (isTime: boolean) => { vscode.commands.executeCommand('setContext', 'extension:isTime', isTime); };
	const setExperienceContext = async (isExperience: boolean) => { vscode.commands.executeCommand('setContext', 'extension:isExperience', isExperience); };

	const setOpenerTrue = () => { context.globalState.update('opener', true); setOpenerContext(true); };
	const setDepthTrue = () => { context.globalState.update('depth', true); setDepthContext(true); };
	const setReferencesTrue = () => { context.globalState.update('references', true); setReferencesContext(true); };
	const setTimeTrue = () => { console.log("time true"); context.globalState.update('time', true); setTimeContext(true); };
	const setExperienceTrue = () => { console.log("exp true"); context.globalState.update('experience', true); setExperienceContext(true); };

	const setOpenerFalse = () => { context.globalState.update('opener', false); setOpenerContext(false); };
	const setDepthFalse = () => { context.globalState.update('depth', false); setDepthContext(false); };
	const setReferencesFalse = () => { context.globalState.update('references', false); setReferencesContext(false); };
	const setTimeFalse = () => { console.log("time false"); context.globalState.update('time', false); setTimeContext(false); };
	const setExperienceFalse = () => { console.log("exp false"); context.globalState.update('experience', false); setExperienceContext(false); };

	context.subscriptions.push(vscode.commands.registerCommand('extension.TrustedCodingParse', disposable));
	context.subscriptions.push(vscode.commands.registerCommand('extension.clearResults', clearResults));
	context.subscriptions.push(vscode.commands.registerCommand('extension.openertrue', setOpenerTrue));
	context.subscriptions.push(vscode.commands.registerCommand('extension.openerfalse', setOpenerFalse));
	context.subscriptions.push(vscode.commands.registerCommand('extension.depthtrue', setDepthTrue));
	context.subscriptions.push(vscode.commands.registerCommand('extension.depthfalse', setDepthFalse));
	context.subscriptions.push(vscode.commands.registerCommand('extension.referencestrue', setReferencesTrue));
	context.subscriptions.push(vscode.commands.registerCommand('extension.referencesfalse', setReferencesFalse));
	context.subscriptions.push(vscode.commands.registerCommand('extension.timetrue', setTimeTrue));
	context.subscriptions.push(vscode.commands.registerCommand('extension.timefalse', setTimeFalse));
	context.subscriptions.push(vscode.commands.registerCommand('extension.experiencetrue', setExperienceTrue));
	context.subscriptions.push(vscode.commands.registerCommand('extension.experiencefalse', setExperienceFalse));
	context.subscriptions.push(vscode.commands.registerCommand('extension.openbrowser', openBrowser) );

	const opener = context.globalState.get('opener', false);
	const depth = context.globalState.get('depth', false);
	const references = context.globalState.get('references', false);
	const time = context.globalState.get('time', false);
	const script_time = context.globalState.get('script_time', 0);
	const experience = context.globalState.get('experience', false);
	const experience_time = context.globalState.get('experience_time', 0);

	if (opener) { setOpenerTrue(); } else { setOpenerFalse(); }
	if (depth) { setDepthTrue(); } else { setDepthFalse(); }
	if (references) { setReferencesTrue(); } else { setReferencesFalse(); }
	if (time) { setTimeTrue(); } else { setTimeFalse(); }
	if (experience) { setExperienceTrue(); } else { setExperienceFalse(); }
}

async function parse_call(context: vscode.ExtensionContext) {
	const activeEditor = vscode.window.activeTextEditor;
	if (!activeEditor) {
		return;
	}

	const source = activeEditor.document.getText().toString();
	if (!source) {
		vscode.window.showInformationMessage('Source code not found');
		return;
	}

	const language = activeEditor.document.languageId;
	if (language != 'php' && language != 'python') {
		vscode.window.showInformationMessage("Language must be PHP or Python: '" + language + "' detected");
		return;
	}

	const user = context.globalState.get('user', '');
	const depth = context.globalState.get('depth', '');
	const references = context.globalState.get('references', '');
	const levels = context.globalState.get('levels', []);
	const tool = context.globalState.get('tool', '');
	const opener = context.globalState.get('opener', '');
	const time = context.globalState.get('time', false);
	const experience = context.globalState.get('experience', false);

	var script_time = context.globalState.get('script_time', 0);
	if (time) {
		if (script_time === 0) {
			var tmp: any;
			const scriptingMinutes = await vscode.window.showInputBox(
				{
					placeHolder: 'time',
					prompt: 'Scripting Time (minutes)'
				}
			).then((data)=>{
				tmp = parseInt(data!);
				if ( isNaN(tmp) ) { tmp = 0; }
			});
			context.globalState.update('script_time', tmp);
		}
		script_time = context.globalState.get('script_time', 0);
	}

	var experience_time = context.globalState.get('experience_time', 0);
	if (experience) {
		if (experience_time === 0) {
			var tmp: any;
			const experienceYears = await vscode.window.showInputBox(
				{
					placeHolder: 'experience',
					prompt: 'Experience Time (Years)'
				}
			).then((data)=>{
				tmp = parseInt(data!);
				if ( isNaN(tmp) ) { tmp = 0; }
			});
			context.globalState.update('experience_time', tmp);
		}
		experience_time = context.globalState.get('experience_time', 0);
	}


	const url = "https://trustedcoding.pre.inftel.madisonmk.com/api/v1/sast/code";

	const data = {
		"source": source,
		"language": language,
		"user": user,
		"levels": levels,
		"depth": depth,
		"references": references,
		"time": script_time,
		"experience": experience_time
	};

	postData(context, url, data);
	context.globalState.update('script_time', 0);
	context.globalState.update('experience_time', 0);
}

function postData(my_context: Context, url = '', data: { source: string; language: string; user: string; levels: string[]; depth: string, references: string, time:number, experience: number }) {
	vscode.window.showInformationMessage('Analysis start');
	const response = fetch(url, {
		method: 'POST',
		body: JSON.stringify(data),
		headers: { 'Content-Type': 'application/json' },
	}).then(res => res.json())
		.then(res => {
			const html_content: string = popupGenerator(my_context, res)!;

			if (my_context.globalState.get('opener', '')) {
				const panel = vscode.window.createWebviewPanel('trustedCoding', 'Trusted Coding', vscode.ViewColumn.One);
				panel.webview.html = html_content;
			} else {
				const panel = vscode.window.createWebviewPanel('trustedCoding', 'Trusted Coding', vscode.ViewColumn.Beside);
				panel.webview.html = html_content;
			}
		});
}

function popupGenerator(my_context: Context, result: { message: string; fullcode: string; line: string; severity: string; references: [] }[]) {
	const editor = vscode.window.activeTextEditor;
	if (!editor) { return; }

	const options_low = { backgroundColor: '#9bbde5', borderWidth: '1px', opacity: '95', borderStyle: 'solid', overviewRulerColor: 'white', overviewRulerLane: vscode.OverviewRulerLane.Right, light: { backgroundColor: '#cce5ff', borderColor: 'darkblue' }, dark: { backgroundColor: '#006fe6', borderColor: 'lightblue' } };
	const options_medium = { backgroundColor: '#efc295', borderWidth: '1px', opacity: '70', borderStyle: 'solid', overviewRulerColor: 'blue', overviewRulerLane: vscode.OverviewRulerLane.Right, light: { backgroundColor: '#ffd9b3', borderColor: 'darkorange' }, dark: { backgroundColor: '#cc6600', borderColor: 'lightorange' } };
	const options_high = { backgroundColor: 'red', borderWidth: '1px', opacity: '50', borderStyle: 'solid', overviewRulerColor: 'blue', overviewRulerLane: vscode.OverviewRulerLane.Right, light: { backgroundColor: '#ff8080', borderColor: 'darkred' }, dark: { backgroundColor: '#cc0000', borderColor: 'lightred' } };

	let div_style = 'border-radius: 5px; border: 2px solid; padding: 0px; margin: 5px 0px 0px 0px;';
	let div_style_severity = 'border: 3px solid; padding: 2px 10px 2px 10px;';
	const div_style_content = 'padding: 10px;';

	let html_content = "";

	const language = editor.document.languageId;

	if (result.length > 0) {
		for (const i of result) {
			const fullcode = "";
			const code = i.fullcode;
			const message = i.message;
			const severity = i.severity;
			const line = parseInt(i.line);
			let references = i.references;
			let severity_id = 0;
			let decoration: vscode.TextEditorDecorationType;
			const range = new vscode.Range(line - 1, 0, line - 1, 1000);

			if (references == undefined) { references = []; }

			// SEVERITIES CONFIGS
			if (severity == 'LOW') {
				div_style = div_style + 'border-color: #25add4;';
				div_style_severity = div_style_severity + 'background-color: #25add4; border-color: #25add4;';
				decoration = vscode.window.createTextEditorDecorationType(options_low);
				severity_id = 2;
			} else if (severity == "MEDIUM") {
				div_style = div_style + 'border-color: #e19f56;';
				div_style_severity = div_style_severity + 'background-color: #e19f56; border-color: #e19f56;';
				decoration = vscode.window.createTextEditorDecorationType(options_medium);
				severity_id = 1;
			} else {
				div_style = div_style + 'border-color: #c13839;';
				div_style_severity = div_style_severity + 'background-color: #c13839; border-color: #c13839;';
				decoration = vscode.window.createTextEditorDecorationType(options_high);
				severity_id = 0;
			}

			// DECORATOR: Text colors and backgrounds
			editor.setDecorations(decoration, [{ range }]);
			decoratorsCollections.push(decoration);

			// DIAGNOSTIC: Hover effects
			if (!my_context.globalState.get('opener', '')) {
				const collection = createDiagnosticMessage(code, message, line - 1, line - 1, 0, 1000, severity_id, language, references);
				diagnosticCollections.push(collection);
			}

			// HTML: WebView panel rendered HTML
			html_content = html_content + "<div style='" + div_style + "'>";
			html_content = html_content + "<div style='" + div_style_severity + "'><b>" + severity + "</b></div>";
			html_content = html_content + "<div style='" + div_style_content + "'>";
			html_content = html_content + "<b><i>Line:</b> " + line + "</i>";
			html_content = html_content + "<p>" + message + "</p>";
			//html_content = html_content + "<b>Full Code:</b> <br><pre>" + fullcode + "</pre>";

			if (references.length > 0) {
				html_content = html_content + "<b>References:</b><br>";
				html_content = html_content + "<ul>";
				for (const refer of references) {
					html_content = html_content + "<li><a href='" + refer["link"] + "'>" + refer["methodology"] + " " + refer["internal_id"] + "</a> " + refer["name"] + "</li>";
				}
				html_content = html_content + "</ul>";
			}
			html_content = html_content + "</div></div>";
		}
	} else {
		div_style = div_style + 'border-color: green;';
		div_style_severity = div_style_severity + 'background-color: #c13839; border-color: green;';

		html_content = html_content + "<div style='" + div_style + "'>";
		html_content = html_content + "		<div style='" + div_style_severity + "'><b>Success!</b></div>";
		html_content = html_content + "		<div style='" + div_style_content + "'>No errors found! Nice job! :)</div>";
		html_content = html_content + "</div>";
	}
	return html_content;
}

function createDiagnosticMessage(code: string, message: string, lineStart: number, lineEnd: number, colStart: number, colEnd: number, severity: number, language: string, references: []) {

	const collection = vscode.languages.createDiagnosticCollection(new Date().toLocaleString());
	const editor = vscode.window.activeTextEditor;
	const refs = [];
	let j = 0;
	for (const r of references) {
		refs[j] = new vscode.DiagnosticRelatedInformation(
			new vscode.Location(
				vscode.Uri.parse(r["link"]),
				new vscode.Range(new vscode.Position(lineStart, 0), new vscode.Position(lineStart, 1000))
			),
			// MESSAGE
			"" + r["link"] + "\nmetodology: " + r["methodology"] + "\n" + r["methodology"] + " ID: " + r["internal_id"] + "\nname: " + r["name"]
		);
		j++;
	}
	if (editor) {
		const document = editor.document;
		collection.clear();

		collection.set(document.uri, [{
			code: message,
			message: code,
			range: new vscode.Range(
				new vscode.Position(lineStart, colStart),
				new vscode.Position(lineEnd, colEnd)
			),
			severity: severity,
			source: language,
			relatedInformation: refs
		}]);
	}
	return collection;
}